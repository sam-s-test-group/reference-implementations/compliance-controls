# Compliance Controls

This project contains files related to setting up compliance controls for an example project and group.

Look at the yaml file that contains the definitions that will be used for the [Compliance Framework](https://docs.gitlab.com/ee/user/project/settings/index.html#compliance-frameworks).
